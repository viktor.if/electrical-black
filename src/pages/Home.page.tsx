import { CommentCreator, CommentTable } from '../components';
import { useGetCommentList } from '../hooks';

export const HomePage = () => {
  const { data } = useGetCommentList();

  return (
    <>
      <CommentCreator />
      <CommentTable data={data} />
    </>
  );
};
