export interface IComment {
  id: number;
  text: string;
}

export interface ICommentTableProps {
  data: IComment[];
}
