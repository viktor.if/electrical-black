import { useSWRConfig } from 'swr';

const removeComment = async (id: number) => {
  await fetch(`http://localhost:4000/comments/${id}`, {
    method: 'DELETE',
  });
};

export const useDeleteComment = () => {
  const { mutate } = useSWRConfig();

  const deleteComment = (id: number) => {
    mutate('http://localhost:4000/comments', removeComment(id), {
      revalidate: true,
    });
  };

  return { deleteComment };
};
