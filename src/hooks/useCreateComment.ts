import { useSWRConfig } from 'swr';

const postNewComment = async (values: any) => {
  await fetch('http://localhost:4000/comments', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(values),
  });
};

export const useCreateComment = () => {
  const { mutate } = useSWRConfig();

  const createComment = (text: string) => {
    mutate('http://localhost:4000/comments', postNewComment({ text }), {
      revalidate: true,
    });
  };

  return { createComment };
};
