import useSWR from 'swr';

import { IComment } from '../types';

export const useGetCommentList = () => {
  const { data = [] } = useSWR<IComment[]>('http://localhost:4000/comments');

  return { data };
};
