import { PageHeader } from 'antd';
import { HomePage } from './pages/Home.page';

function App() {
  return (
    <div>
      <PageHeader title="My Jira Ticket" ghost={true} className="page-header" />
      <HomePage />
    </div>
  );
}

export default App;
