import { Modal, Button, Table } from 'antd';
import { DeleteFilled } from '@ant-design/icons';
import { ColumnsType } from 'antd/lib/table';

import { IComment, ICommentTableProps } from '../types';
import { useDeleteComment } from '../hooks';

const { confirm } = Modal;

const showConfirm = (onOk: () => void) => {
  confirm({
    title: 'Delete the comment',
    icon: <DeleteFilled color="#333" />,
    content: 'Are you sure you want to delete this comment?',
    onOk,
  });
};

export const CommentTable = ({ data }: ICommentTableProps) => {
  const { deleteComment } = useDeleteComment();

  const onDelete = (id: number) => {
    showConfirm(() => deleteComment(id));
  };

  const columns: ColumnsType<IComment> = [
    {
      title: 'Id',
      dataIndex: 'id',
    },
    {
      title: 'Comment',
      dataIndex: 'text',
    },
    {
      title: 'Action',
      align: 'right',
      render: (_text, record) => (
        <Button
          type="primary"
          danger
          icon={<DeleteFilled />}
          onClick={() => onDelete(record.id)}
        >
          DELETE
        </Button>
      ),
    },
  ];

  return (
    <Table
      dataSource={data}
      columns={columns}
      rowKey="id"
      className="comment-table"
    />
  );
};
