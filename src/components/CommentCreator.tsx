import { Form, Input, Button, message } from 'antd';
import { PlusCircleFilled } from '@ant-design/icons';
import { useCreateComment } from '../hooks';

export const CommentCreator = () => {
  const [form] = Form.useForm();
  const { createComment } = useCreateComment();

  const onFinish = (values: any) => {
    form.resetFields();
    message.info('New comment has been added');
    createComment(values.text);
  };

  return (
    <Form form={form} className="form" onFinish={onFinish}>
      <Form.Item
        name="text"
        rules={[{ required: true, message: 'Please input your username!' }]}
      >
        <Input placeholder="Comment" />
      </Form.Item>
      <Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          className="form-submit"
          icon={<PlusCircleFilled color="#fff" />}
        >
          ADD COMMENT
        </Button>
      </Form.Item>
    </Form>
  );
};
